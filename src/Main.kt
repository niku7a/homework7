fun main() {

    val s = sayHello("Jemali")
    println(s)

    val k = sayHello()
    println(k)

}

fun sayHello(name: String = "Guliko"): String {
    return "Hello $name"
}